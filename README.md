**Got a question, a suggestion or an idea ?!** Contact me on [contact.kevinmasson@gmail.com](mailto:contact.kevinmasson@gmail.com)

## Introduction

km_scriptFinder is an [After Effects](http://www.adobe.com/products/aftereffects.html) script that allows you to quikly and easily work with After Effects scripts.

## Installation

Copy `scriptFinder.jsx` to `YourAfterEffectsDirectory/Support Files/Scripts/ScriptUI Panels`

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

If you are looking for help for developping script on After Effects, be sure to check out the [After Effects  Developer Center](http://www.adobe.com/devnet/aftereffects.html), the [Coding Corner Community](https://forums.adobe.com/community/coding-corner) and if you want, you can contact me.

## Features

Up to now the list of functionality is pretty short but is will grow up.

- Add one or multiple jsx scripts to your Script Finder
- Add one or multiple jsxinc scripts to your Script Finder
- Launch a script of your Script Finder

## Licence

This project is under [GNU GENERAL PUBLIC LICENSE Version 3](https://github.com/kevinmasson/km_scriptFinder/blob/master/LICENSE).
