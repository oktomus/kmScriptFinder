alert("It is working !");

(function(glob){
  alert("Hey, it also works with IIFE functions !");
})(this);

/*
Here is a comment
alert("this won't be executed");
*/
alert("Comments doesn't cause issues");

// The script doesn't have access to the launcher scope
km.after.app.specialLog("Something");
