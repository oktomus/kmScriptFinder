/*
km_scriptFinder - A script that helps you to execute scripts in After Effects.
Copyright (C) 2016  Kevin Masson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact the author by mail : contact.kevinmasson@gmail.com

*/

(function (global)
{
  /**
  * KM Namespace
  */
  var km = km || {
    after : {
      scriptFinder : {},
      script : {},
      app : {}
    },
    ui : {
      debug : {},
      scriptFinder : {}
    }
  };

  /**
  * KM App
  */

  km.after.app = (function(){
    var debug = false;
    var version = 1.0;
    var os;
    var panel;
    var main;
    return {
      error : function(e) {
        alert("Error : " + e);
      },

      log : function(message){
        writeLn(message);
      },

      debugLog : function(message) {
        if(this.isDebug()){
          writeLn(message);
        }
      },

      specialLog : function(message) {
        alert(message);
      },

      isDebug : function() {
        return this.debug;
      },

      isOnMac : function(){
        return os === "MAX";
      },

      activateDebug : function() {
        km.after.app.debugLog("Enabling debuging");
        this.debug = true;
      },

      stopDebug : function() {
        this.debug = false;
      },

      run : function() {
        clearOutput();
        this.os = $.os.toLowerCase().indexOf('mac') >= 0 ? "MAC": "WINDOWS";
        km.after.app.activateDebug();
        km.after.scriptFinder.init();
        km.after.scriptFinder.show();
        km.ui.scriptFinder.init(km.after.scriptFinder);
      }
    };
  })();

  /**
  * Script package
  */

  km.after.script = (function(){
    return {
      /**
      * Script File
      * @constructor
      */
      ScriptFile : function ScriptFile(name, path) {
        km.after.app.debugLog("Creating a script file named " + name + "...");
        // To be sure that ScriptFile is called as new and not a function

        this.fileName = name;
        this.filePath = path;

        /**
        * Launcht the script by evaluating it
        */
        this.launch = function() {

          var path = this.filePath;
          (function(){
            $.evalFile(path);
          })();

          km.after.app.debugLog("Executed");


        };

        this.show = function() {
          return this.fileName;
        };

        km.after.app.debugLog("Script file created.");
      }
    };
  })();


  /**
  * Script Finder
  */

  km.after.scriptFinder = (function() {
    var multiSelect;
    var scriptFiles;
    return {
      /**
      * Initialisation of the sript finder
      * @constructor
      */
      init : function(){
        km.after.app.debugLog("Intisialazing the scriptfinder");
        this.multiSelect=true;
        this.scriptFiles = [];
      },

      /**
      * Remove a script from
      * @param {String} name - The name of the script we want to remove
      * @return {Boolean} - True if the script was correctly removed
      */
      remove : function(name){
        var index = this.scriptIndex(name);
        if(index==-1) throw "Error 201 : The script " + name + " is not in the scripts list.";
        this.scriptFiles.splice(index, 1);
        return true;
      },

      /**
      * Show the scriptFinder state
      */
      show : function(){
        var scripts = "";
        for (var index in this.scriptFiles) {
          if (this.scriptFiles.hasOwnProperty(index)) {
            scripts += String(this.scriptFiles[index].show()) + ", ";
          }
        }
        km.after.app.debugLog("Script Finder { multiSelect: " + String(this.multiSelect) + ", scriptFiles: " + scripts + "}" );
      },

      /**
      * Find the index of a script with its name
      *
      * @param {String} name - The name of the script we are looking for
      * @return index - the index in scriptfiles of the script
      */
      scriptIndex : function(name){
        index = -1;
        for(var i = 0, len = this.scriptFiles.length; i < len; i++) {
          if (this.scriptFiles[i].fileName === name) {
            index = i;
            break;
          }
        }
        return index;
      },

      /**
      * Execute a script of the scripts list
      *
      * @param {String} name - the name of the script
      */
      execute : function(name){
        var index = this.scriptIndex(name);
        if(index==-1) throw "Error 201 : The script " + name + " is not in the scripts list.";
        km.after.app.debugLog("Executing script " + name + ". Its index is " + index);
        this.scriptFiles[index].launch();
      },

      /**
      * Add a script to the list
      *
      * @param {File} file - The file to create a scriptFile and to add
      */
      add : function(file){
        km.after.app.debugLog("Adding file " + file.name + " to scripts files.");
        km.after.app.debugLog("Location : " + file.fullName);
        var scriptFile = new km.after.script.ScriptFile(file.name, file.fullName);
        if(!(scriptFile instanceof km.after.script.ScriptFile)){
          throw "The script file cannot be instantiated";
        }
        this.scriptFiles.push(scriptFile);
        return scriptFile.show();
      },

      /**
      * Open a OS dialog to select the script(s) we want to add to our script finder
      */
      openDialog : function(){
        km.after.app.debugLog("Openning dialog.");
        var filter = "JavaScript:*.jsx;*.jsxinc";
        if(km.after.app.isOnMac()){
          filter = function(file){
            if (file.constructor.name == "Folder") { return true; }
            if (file.name.slice(-4) == ".jsx" || file.name.slice(-7) == ".jsxinc") { return true; }
            return false;
          };
        }
        var files = File.openDialog("Choose a script", filter, multiSelect=this.multiSelect);
        var addedFiles = [];
        for (var index in files) {
          addedFiles.push(this.add(files[index]));
        }
        return addedFiles;
      }



    };
  })();

  /**
  * Script Finder UI
  */
  km.ui.scriptFinder = (function(){
    // The model
    var scriptFinder;
    var dropDown;
    var scriptFinderPalette;
    var bAdd;
    var bDel;
    var bRun;
    return {
      init : function(model){
        this.scriptFinder = model;
        this.initUi();
      },

      addItem : function(){
        km.after.app.debugLog("Before adding ----");
        this.scriptFinder.show();
        files = this.scriptFinder.openDialog();
        for (var index in files) {
          if (files.hasOwnProperty(index)) {
            item = files[index];
            this.dropDown.add('item', item);
            km.after.app.debugLog("Added " + item + " to the dropdown");
          }
        }
        km.after.app.debugLog("After adding ----"); this.scriptFinder.show();
        this.updateState();
      },

      deleteItem : function(){
        var  name = this.dropDown.selection.text;
        if(this.scriptFinder.remove(name)){
          var id = this.dropDown.selection.index;
          this.dropDown.remove(id);
          this.updateState();
          this.dropDown.selection = 0;
        }
      },

      updateState : function(){
        if(this.dropDown.items.length === 0){
          this.dropDown.enabled = false;
          this.bRun.Senabled = false;
          this.bDel.enabled = false;
        }else{
          this.dropDown.enabled = true;
          this.bRun.enabled = true;
          this.bDel.enabled = true;
        }
      },

      run : function(){
        var  name = this.dropDown.selection.text;
        this.scriptFinder.execute(name);
      },

      initUi : function() {
        //=================================
        //===== SCRIPT FINDER PALETTE =====
        //=================================
        this.scriptFinderPalette = (this.scriptFinderPalette instanceof Panel) ? this.scriptFinderPalette : new Window('palette','Script Finder', undefined, {resizeable:true});
        this.scriptFinderPalette.orientation = 'stack';
        //=================================
        //===== PALETTE BUTTONS =====
        //=================================
        var mGr = this.scriptFinderPalette.add('group');
        mGr.orientation = 'column';
        var dd = mGr.add("dropdownlist", undefined);
        dd.selection = 0;
        this.dropDown = dd;
        this.bRun = mGr.add("button", undefined, "Run");
        this.bAdd = mGr.add("button", undefined, "Add");
        this.bDel = mGr.add("button", undefined, "Delete");
        //=================================
        //===== BUTTONS FUNCTION =====
        //=================================
        this.bAdd.onClick =function(){
          try {
            km.after.app.debugLog("Add button clicked");
            km.ui.scriptFinder.addItem();
          } catch (e) {
            km.after.app.error(e);
          }

        };

        this.bRun.onClick = function(){
          try {
            km.ui.scriptFinder.run();
          } catch (e) {
            km.after.app.error(e);
          }
        };

        this.bDel.onClick = function(){
          try {
            km.ui.scriptFinder.deleteItem();
          } catch (e) {
            km.after.app.error(e);
          }
        }
        //=================================
        //===== PALETTE DISPLAY =====
        //=================================
        this.scriptFinderPalette.layout.resize();
        this.scriptFinderPalette.center();
        this.scriptFinderPalette.show();
        this.scriptFinderPalette.onResizing = this.scriptFinderPalette.onResize = function () { this.layout.resize(); };
        this.updateState();

      }
    };
  })();

  // START

  try{
    km.after.app.run();
  }catch(err){
    var issue = err.toString();
    alert("Internal Error : " + issue);
  }


})(this);
